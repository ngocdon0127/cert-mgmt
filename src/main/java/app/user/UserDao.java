package app.user;

import com.google.common.collect.*;
import com.google.gson.Gson;

import app.util.FileUtil;
import app.util.UserUtil;

import java.nio.file.Paths;

import java.util.*;
import java.util.stream.*;

public class UserDao {

    private static Gson gson = new Gson();

    private List<User> users = new ArrayList();

    public User getUserByUsername(String username) {
        // if (this.users == null) {
            this.users = UserUtil.readUsers();
        // }
        return users.stream().filter(b -> b.getUsername().equals(username)).findFirst().orElse(null);
    }

    public Iterable<String> getAllUserNames() {
        // if (this.users == null) {
            this.users = UserUtil.readUsers();
        // }
        return users.stream().map(User::getUsername).collect(Collectors.toList());
    }

}
