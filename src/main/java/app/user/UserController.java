package app.user;

import org.mindrot.jbcrypt.*;
import static app.Application.userDao;

public class UserController {

    public static boolean authenticate(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return false;
        }
        User user = userDao.getUserByUsername(username);
        if (user == null) {
            return false;
        }
        String hashedPassword = BCrypt.hashpw(password, user.getSalt());
        return hashedPassword.equals(user.getHashedPassword());
    }

    // reset password
    public static void setPassword(String username, String oldPassword, String newPassword) {
        if (authenticate(username, oldPassword)) {
            String newSalt = BCrypt.gensalt();
            String newHashedPassword = BCrypt.hashpw(newPassword, newSalt);
            // Update the user salt and password
        }
    }

    public static boolean isCAManager(String username){
        User user = userDao.getUserByUsername(username);
        if (user != null && user.getIsCAManager() == 1 ) {
            System.out.println("manager");
            return true;
        }
        System.out.println("bxdinh");
        return false;
    }
}
