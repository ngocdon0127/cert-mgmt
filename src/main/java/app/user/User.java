package app.user;

public class User {
    private String username;
    private String salt;
    private String hashedPassword;
    private Integer isCAManager;

    public Integer getIsCAManager() {
        return isCAManager;
    }

    public void setIsCAManager(Integer isCAManager) {
        this.isCAManager = isCAManager;
    }

    public User(String username, String salt, String hashedPassword, int isCAManager) {
        this.username = username;
        this.salt = salt;
        this.hashedPassword = hashedPassword;
        this.isCAManager = isCAManager;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
}
