package app.download;

import app.certificate.CertificateController;
import app.login.LoginController;
import app.util.FileUtil;
import app.util.Param;
import spark.Request;
import spark.Response;
import spark.Route;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DownloadController {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static Route getFile = (Request request, Response response) -> {
        LoginController.ensureUserIsLoggedIn(request, response);
        System.out.println(CertificateController.getCertFileName());
        File certFile = new File(
                Paths.get(Param.CERT_PATH, CertificateController.getCertFileName(),
                        "Certificate.json").toString());

        File rFile = new File(
                Paths.get(Param.CERT_PATH, CertificateController.getCertFileName(),
                        "r.json").toString());

        if (!certFile.exists() || !rFile.exists()) {
            System.out.println("file download not exist");
        }
        System.out.println("file exist!");

        List<File> fileList = new ArrayList<>();
        fileList.add(certFile);
        fileList.add(rFile);
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition","attachment; filename=Certificates.zip");
        try {
            try(ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new BufferedOutputStream(response.raw().getOutputStream())))
            {
                for (File file : fileList) {
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                    ZipEntry zipEntry = new ZipEntry(file.getName());
                    zipOutputStream.putNextEntry(zipEntry);
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = bufferedInputStream.read(buffer)) > 0) {
                        zipOutputStream.write(buffer, 0, len);
                    }
                    zipOutputStream.flush();
                }
            }
        } catch (Exception e) { System.out.println("e = " + e); }
        //CertificateController.deleteCreatedCert();
       return response.raw();
    };

    public static Route handleCRLDownload = (Request request, Response response) -> {
        File crlFile = new File(Param.CRL_PATH);
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition","attachment; filename=CRL.zip");
        try {
            try(ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new BufferedOutputStream(response.raw().getOutputStream())))
            {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(crlFile));
                ZipEntry zipEntry = new ZipEntry(crlFile.getName());
                zipOutputStream.putNextEntry(zipEntry);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bufferedInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                zipOutputStream.flush();
            }
        } catch (Exception e) { System.out.println("e = " + e); }
        return response.raw();
    };

    

    public static Route handleCAPubDownload = (Request request, Response response) -> {
        File crlFile = new File(Param.CA_PUB_PATH);
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition", "attachment; filename=ca-pub.zip");
        try {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new BufferedOutputStream(response.raw().getOutputStream()))) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(crlFile));
                ZipEntry zipEntry = new ZipEntry(crlFile.getName());
                zipOutputStream.putNextEntry(zipEntry);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bufferedInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                zipOutputStream.flush();
            }
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return response.raw();
    };

    public static Route handleCertificatesDownload = (Request request, Response response) -> {
        File certificatesFolder = new File(
                Paths.get(Param.CERT_PATH).toString());
        File[] folders = certificatesFolder.listFiles();

        List<File> fileList = new ArrayList<>();
        List certificateInfos = new ArrayList<Map>();

        for(File folder : folders) {
            if (folder.exists() && folder.isDirectory()) {
                File certFile = new File(
                        Paths.get(Param.CERT_PATH, folder.getName(), "Certificate.json").toString());
                if (certFile.exists()) {
                    String content = FileUtil.readWholeFile(Paths.get(Param.CERT_PATH, folder.getName(), "Certificate.json").toString());
                    Map m = gson.fromJson(content, HashMap.class);
                    certificateInfos.add(m);
                }
            }
        }

        FileUtil.writeFile(gson.toJson(certificateInfos), 
                Paths.get(System.getProperty("user.dir"), "download", "Certificates.json").toString());

        // File rFile = new File(Paths.get(Param.CERT_PATH, CertificateController.getCertFileName(), "r.json").toString());

        // if (!certFile.exists() || !rFile.exists()) {
        //     System.out.println("file download not exist");
        // }
        // System.out.println("file exist!");

        
        // fileList.add(certFile);
        // fileList.add(rFile);
        File mergedCertificatesFile = new File(Paths.get(System.getProperty("user.dir"), "download", "Certificates.json").toString());
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition", "attachment; filename=Certificates.zip");
        try {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new BufferedOutputStream(response.raw().getOutputStream()))) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(mergedCertificatesFile));
                ZipEntry zipEntry = new ZipEntry(mergedCertificatesFile.getName());
                zipOutputStream.putNextEntry(zipEntry);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bufferedInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                zipOutputStream.flush();
            }
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return response.raw();
    };

    public static Route handleCertificateClientDownload = (Request request, Response response) -> {
        File crlFile = new File(Param.CERT_CLIENT_PATH);
        response.raw().setContentType("application/octet-stream");
        response.raw().setHeader("Content-Disposition", "attachment; filename=certificate-client.zip");
        try {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new BufferedOutputStream(response.raw().getOutputStream()))) {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(crlFile));
                ZipEntry zipEntry = new ZipEntry(crlFile.getName());
                zipOutputStream.putNextEntry(zipEntry);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = bufferedInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                zipOutputStream.flush();
            }
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return response.raw();
    };
}
