package app.index;

import app.login.LoginController;
import app.util.*;
import spark.*;
import java.util.*;
import static app.Application.*;

public class StartController {
    public static Route serveStartPage = (Request request, Response response) -> {
        // LoginController.ensureUserIsLoggedIn(request, response);
        Map<String, Object> model = new HashMap<>();
        
        return ViewUtil.render(request, model, Path.Template.START);
    };
}
