package app.certificaterevocationlist;

import app.certificate.CertificateDao;
import app.util.CAKey;
import app.util.Param;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.*;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CertificateRevocationDao {

    //get all certificate was revoked
    public static Iterable<CertificateRevocation> getAllCR() throws Exception {
        return readCertificateRevocation();
    }

    //return a revoked certificate index
    public static int getCRIndexbySerial(String serial) throws Exception{
        List<CertificateRevocation> list = readCertificateRevocation();
        if (list == null){
            return -1;
        }
        int len = list.size(), i;
        for (i = 0; i < len; i++) {
            if (CertificateDao.getSerialString(list.get(i).getSerial()).equals(serial)){
                return i;
            }
        }
        return -1;
    }

    //add IC to CRL
    public static void addCertificateRevocation(CertificateRevocation cr) throws Exception{
        CertificateRevocationList crl = new CertificateRevocationList();
        crl = readCRLFromFile();
        if (crl == null) {
            crl = new CertificateRevocationList();
            List<CertificateRevocation> list = new ArrayList<>();
            list.add(cr);
            crl.setList(list);
            crl.setSignature(BigInteger.ZERO);
            crl.setCurve("SECP192R1");
            crl.setHash("SHA1withECDSA");
        }
        else {
            crl.getList().add(cr);
        }
        crl = generateSignature(crl);
        writeCRLtoFile(crl);
    }

    //read crl object form json file.
    public static CertificateRevocationList readCRLFromFile() throws Exception {
        List<CertificateRevocation> list = new ArrayList<>();
        File file = new File(Param.CRL_PATH);
        if (!file.exists()) {
            return null;
            //write scheam to CLR file
        }
        InputStream is = new FileInputStream(Param.CRL_PATH);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject jsonObject = jsonReader.readObject();
        if (jsonObject == null){
            return null;
        }
        jsonReader.close();
        is.close();
        CertificateRevocationList crl = new CertificateRevocationList();
        crl.setCurve(jsonObject.getString("Curve"));
        crl.setHash(jsonObject.getString("Hash"));
        crl.setSignature(new BigInteger( jsonObject.getString("Signature"), 16));
        JsonArray jsonArray = jsonObject.getJsonArray("crl");
        DateFormat format = new SimpleDateFormat("E MMMM dd hh:mm:ss zzz yyyy");
        for (JsonValue jsonValue : jsonArray){
            CertificateRevocation cr = new CertificateRevocation();
            cr.setSerial(new BigInteger(jsonValue.asJsonObject().getString("Serial"),16));
            cr.setTime(format.parse(jsonValue.asJsonObject().getString("Time")));
            cr.setReason(jsonValue.asJsonObject().getString("Reason"));
            list.add(cr);
        }
        crl.setList(list);
        return crl;
    }

    //return list of revoked IC
    public static List<CertificateRevocation> readCertificateRevocation() throws Exception{
        if (readCRLFromFile() == null){
            return null;
        }
        return readCRLFromFile().getList();
    }

    //generate signature for CRL
    //input: crl with old sign, output: crl with new sign
    private static CertificateRevocationList generateSignature(CertificateRevocationList crl) throws Exception{
        List<CertificateRevocation> list = crl.getList();
        JsonArrayBuilder arrayBuilders = createArrayBuilder(list);
        JsonArray root = arrayBuilders.build();
        String conttent = "SECP192R1" + "SHA1withECDSA" + root.toString();
        //generate sign
        Signature ecdsa;
        PrivateKey privKeyCA = CAKey.getCAPrivateKey();
        ecdsa = Signature.getInstance("SHA1withECDSA", "SunEC");
        ecdsa.initSign(privKeyCA);
        byte[] baContent = conttent.toString().getBytes("UTF-8");
        ecdsa.update(baContent);
        byte[] baSignature = ecdsa.sign();
        crl.setSignature(new BigInteger(1, baSignature));
        return crl;
    }

    //delete certificate from CRL
    public static void deleteCertificateRevocation(int index) throws Exception{
        CertificateRevocationList crl = new CertificateRevocationList();
        crl = readCRLFromFile();
        crl.getList().remove(index);
        crl = generateSignature(crl);
        writeCRLtoFile(crl);
    }

    //write crl to file
    public static void writeCRLtoFile(CertificateRevocationList crl) throws Exception{
        List<CertificateRevocation> list = crl.getList();
        JsonArrayBuilder arrayBuilders = createArrayBuilder(list);

        JsonObjectBuilder job = Json.createObjectBuilder();
        job.add("Curve", crl.getCurve());
        job.add("Hash", crl.getHash());
        job.add("Signature", crl.getSignature().toString(16));
        job.add("crl", arrayBuilders);

        JsonObject root = job.build();

        File file = new File(Param.CRL_DIRECTORY);
        if (!file.exists()){
            CertificateDao.fileWithDirectoryAssurance(Param.CRL_DIRECTORY, "CRL.json");
        }
        OutputStream os;
        os = new FileOutputStream(Param.CRL_PATH);

        HashMap config = new HashMap<String, Boolean>();
        config.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory jwf = Json.createWriterFactory(config);
        StringWriter sw = new StringWriter();

        try (JsonWriter jsonWriter = jwf.createWriter(os)) {
            jsonWriter.writeObject(root);
        }
        try (JsonWriter jsonWriter = jwf.createWriter(sw)){
            jsonWriter.writeObject(root);
        }
    }

    private static JsonArrayBuilder createArrayBuilder(List<CertificateRevocation> crl) {
        JsonArrayBuilder arrayBuilders = Json.createArrayBuilder();
        for (int i =0; i < crl.size(); i++){
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            objectBuilder.add("Serial",crl.get(i).getSerialHexString()).
                    add("Time", crl.get(i).getTime().toString()).
                    add("Reason", crl.get(i).getReason());
            arrayBuilders.add(objectBuilder);
        }
        return arrayBuilders;
    }

}
