package app.certificaterevocationlist;

import app.certificate.CertificateDao;
import app.login.LoginController;
import app.user.UserController;
import app.util.Path;
import app.util.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;

import static app.Application.certificateRevocationDao;
import static app.util.JsonUtil.dataToJson;
import static app.util.RequestUtil.*;

public class CertificateRevocationController {

    public static Route fetchAllCR = (Request request, Response response) -> {
        LoginController.ensureUserIsLoggedIn(request, response);
        String currentUser = getSessionCurrentUser(request);
        System.out.println("currentUser = " + currentUser);
        if (UserController.isCAManager(currentUser) == false){
            return ViewUtil.notRight.handle(request, response);
        }

        if (clientAcceptsHtml(request)) {
            HashMap<String, Object> model = new HashMap<>();
            model.put("crl", certificateRevocationDao.getAllCR());
            return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
        }
        if (clientAcceptsJson(request)) {
            return dataToJson(certificateRevocationDao.getAllCR());
        }
        return ViewUtil.notAcceptable.handle(request, response);
    };

    //handle: add CR to CRL
    public static Route handleCRLPost = (Request request, Response response) -> {
        LoginController.ensureUserIsLoggedIn(request,response);
        String currentUser = getSessionCurrentUser(request);
        if (UserController.isCAManager(currentUser) == false){
            return ViewUtil.notRight.handle(request, response);
        }
        HashMap<String, Object> model = new HashMap<>();
        //check serial
        if (!checkCR(getRequestSerial(request))){
            model.put("inputSerial",true);
            model.put("crl", certificateRevocationDao.getAllCR());
            return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
        }
        //serial co nam trong ds nhung serial da cap?
        int rcIndex = CertificateDao.getRCIndexbySerial(getRequestSerial(request));

        if (rcIndex == -1) {
            model.put("DeleteCertIsNotRegistered", true);
            model.put("crl", certificateRevocationDao.getAllCR());
            return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
        }
        //check serial, serial da ton tai trong crl?
        int crIndex = CertificateRevocationDao.getCRIndexbySerial(getRequestSerial(request));

        if (crIndex != -1) {
            model.put("DeleteCRisExisting", true);
            model.put("crl", certificateRevocationDao.getAllCR());
            return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
        }
        String reason = new String();
        BigInteger serial = new BigInteger(getRequestSerial(request),16);
        reason = getRequestReason(request);
        Date time = new Date();
        CertificateRevocation cr = new CertificateRevocation();
        cr.setSerial(serial);
        cr.setTime(time);
        cr.setReason(reason);
        CertificateRevocationDao.addCertificateRevocation(cr);
        model.put("AddCRSuccessed", true);
        model.put("crl",certificateRevocationDao.getAllCR());
        return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
    };

    public static Route handleCRDelete = (Request request, Response response) -> {
        System.out.println("handlerCRDelete");
        LoginController.ensureUserIsLoggedIn(request ,response);
        String currentUser = getSessionCurrentUser(request);
        if (UserController.isCAManager(currentUser) == false){
            return ViewUtil.notRight.handle(request, response);
        }
        HashMap<String, Object> model = new HashMap<>();
         //delete cr in crl file.
        int crIndex = CertificateRevocationDao.getCRIndexbySerial(getParamSerial(request));
        //if certificate was not revoked
        if (crIndex == -1) {
            model.put("DeleteCRFailed", true);
            model.put("crl", certificateRevocationDao.getAllCR());
            return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
        }
        CertificateRevocationDao.deleteCertificateRevocation(crIndex);
        response.redirect(Path.Web.CRL_MANAGEMENT);
//        return null;
        model.put("DeleteCRSuccessed", true);
        model.put("crl", certificateRevocationDao.getAllCR());
        return ViewUtil.render(request, model, Path.Template.CRL_MANAGEMENT);
    };

    //function to check certificate request argument
    public static boolean checkCR(String serial) {
        String pattern = "^[0-9A-Fa-f]+$";
        if (serial.matches(pattern)) {
            return true;
        } else {
            return false;
        }
    }


}
