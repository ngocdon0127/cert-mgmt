package app.certificate;

import app.util.Param;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.*;

public class CertificateDao {

    //return a registered certificate index
    public static int getRCIndexbySerial(String serial) throws Exception{
        List<String> list = CertificateDao.readAllRegisteredCert();
        if (list == null) {
            return -1;
        }
        int len = list.size(), i;
        for (i = 0; i < len; i++) {
            if (list.get(i).equals(serial)){
                return i;
            }
        }
        return -1;
    }

    // generate serial number fo IC
    public static BigInteger generateSerialBigIntegerNumber() {
        SecureRandom rand = new SecureRandom();
        BigInteger upperLimit = new BigInteger("10000000000000000",16); //16 characters "0"
        BigInteger result;
        do {
            result = new BigInteger(upperLimit.bitLength(), rand);
        } while (result.compareTo(upperLimit) >= 0);
        return result;
    }

    //create serial string with 40 charactor
    public static String getSerialString(BigInteger serial){
        String serialStr = serial.toString(16);
        int len = serialStr.length();
        while (len < 16){
            serialStr = '0' + serialStr;
            len += 1;
        }
        return serialStr;
    }

    //write registered cert to json file.
    public static void writeRegisteredCert(BigInteger serial) throws Exception{
        JsonArrayBuilder arrayBuilders = Json.createArrayBuilder();
        List<String> list = new ArrayList<>();
        list = readAllRegisteredCert();
        if (list != null) {
            list.add(getSerialString(serial));
        }else {
            list = new ArrayList<String>();
            list.add(getSerialString(serial));
        }
        for (int i =0; i < list.size(); i++){
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            objectBuilder.add("Serial",list.get(i));
            arrayBuilders.add(objectBuilder);
        }
        JsonArray jsonArray = arrayBuilders.build();

        File file = new File(Param.REGISTED_CERTIFICATE_PATH);
        if (!file.exists()){
            CertificateDao.fileWithDirectoryAssurance(Param.REGISTED_CERTIFICATE_DIRECTORY,
                    "RegistedCertificate.json");
        }

        OutputStream os;
        os = new FileOutputStream(Param.REGISTED_CERTIFICATE_PATH);

        HashMap config = new HashMap<String, Boolean>();
        config.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory jwf = Json.createWriterFactory(config);
        StringWriter sw = new StringWriter();

        try (JsonWriter jsonWriter = jwf.createWriter(os)) {
            jsonWriter.writeArray(jsonArray);
        }
    }

    //read registered cert from file.
    public static List<String> readAllRegisteredCert() throws Exception {
        File file = new File(Param.REGISTED_CERTIFICATE_PATH);
        if (!file.exists()) {
            return null;
        }
        List<String> list = new ArrayList<>();
        InputStream is = new FileInputStream(Param.REGISTED_CERTIFICATE_PATH);
        JsonParserFactory factory = Json.createParserFactory(null);
        JsonParser parser = factory.createParser(is, Charset.defaultCharset() );

        if (!parser.hasNext() && parser.next() != JsonParser.Event.START_ARRAY) {
            return null;
        }
        // looping over object attributes
        while (parser.hasNext()) {
            JsonParser.Event event = parser.next();
            // starting object
            if (event == JsonParser.Event.START_OBJECT) {
                while (parser.hasNext()) {
                    event = parser.next();
                    if (event == JsonParser.Event.KEY_NAME) {
                        String key = parser.getString();
                        switch (key) {
                            case "Serial":
                                parser.next();
                                list.add(parser.getString());
                                break;
                        }
                    }
                }
            }
        }
        return list;
    }

    //function to write json object to json file
    public static void writeJsonToFile(JsonObject jsonObject, File file) throws FileNotFoundException {
        OutputStream os;
        file.getParentFile().mkdirs();
        os = new FileOutputStream(file);
        try (JsonWriter jsonWriter = Json.createWriter(os)) {
            jsonWriter.writeObject(jsonObject);
        }
    }

    //create file with directory
    public static File fileWithDirectoryAssurance(String directory, String filename) {
        File dir = new File(directory);
        if (!dir.exists()) dir.mkdirs();
        return new File(Paths.get(directory, filename).toString());
    }

    //Set the expiration date to one year from the current time
    public static Date generateCertificateDate(Date validFrom){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(validFrom);
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
        Date validTo = calendar.getTime();
        return validTo;
    }

    //function to make JsonObject of IC
    public static JsonObject makeCertificateBuilder (Certificate cert){
        JsonObject jsonObject;
        JsonObjectBuilder tempBuilder = Json.createObjectBuilder();
        tempBuilder.add("MESType", cert.getMesType()).add("SerialNumber", getSerialString(cert.getSerialNumber())).
                add("Curve", cert.getCurve()).add("Hash", cert.getHash()).
                add("Issuer", cert.getIssuerID().toString(16)).
                add("ValidFrom", cert.getValidFrom().toString(16)).
                add("ValidDuration", cert.getValidDuration().toString(16)).
                add("Subject", cert.getSubject().toString(16)).
                add("Usage", String.valueOf(cert.getKeyUsage())).
                add("PU", cert.getPU()).
                add("PublicKeyAlgorithm", cert.getPublicKeyAlgorithm()).
                add("Email", cert.getEmail());
        if (cert.getSignature() != BigInteger.ZERO) {
            tempBuilder.add("Signature", cert.getSignature().toString(16));
        }
        jsonObject = tempBuilder.build();
        return jsonObject;
    }

    public static String makeTBSCertificate (Certificate cert){
        String tbsCert = cert.getMesType() + getSerialString(cert.getSerialNumber()) + cert.getCurve() +
                cert.getHash() + cert.getIssuerID().toString(16) + cert.getValidFrom().toString(16)+
                cert.getValidDuration().toString(16) + cert.getSubject().toString(16) +
                cert.getKeyUsage() + cert.getPU() + cert.getPublicKeyAlgorithm() + cert.getEmail();
        return tbsCert;
    }
}
