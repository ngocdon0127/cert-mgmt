package app.certificate;

import app.certificaterequest.CertificateRequest;
import app.util.CAKey;
import app.util.Param;
import app.util.ScalarMultiply;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.io.File;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.ECPoint;
import java.util.Base64;

public class CertificateController {

    private static Certificate certificate = null;
    public static void generate(CertificateRequest certRequest) throws Exception {
        certificate = new Certificate();
        File outputFile;
        JsonObject rJsonObject, content, fullcontent;
        JsonObjectBuilder builder = Json.createObjectBuilder();

        //get root point G
        ECPoint G = CAKey.getRootPoint();
        PrivateKey privKeyCA = CAKey.getCAPrivateKey();  //private key of CA


        certificate.setMesType("T2");
        certificate.setSerialNumber(CertificateDao.generateSerialBigIntegerNumber());
        certificate.setCurve("SECP192R1");
        certificate.setHash("SHA1withECDSA");
        certificate.setIssuerID(Param.ISSUER);
        long mill = System.currentTimeMillis() / 1000;
        BigInteger validFrom = BigInteger.valueOf(mill);
        BigInteger validDuration = BigInteger.valueOf(mill + 86400*365); //one year license
        certificate.setValidFrom(validFrom);
        certificate.setValidDuration(validDuration);
        certificate.setSubject(certRequest.getCommonName());
        certificate.setKeyUsage(0);  //dont't use: 0
        certificate.setPublicKeyAlgorithm("ECC_192");
        certificate.setEmail(certRequest.getEmail());

        // random k and value of k*G is kG
        BigInteger k = ScalarMultiply.getRandomBigInteger();
        ECPoint kG = ScalarMultiply.scalmult(G, k);
        ECPoint rU = new ECPoint(new BigInteger(certRequest.getXrU().toString(), 10), new BigInteger(certRequest.getYrU().toString(), 10));
        ECPoint pU;
        BigInteger e;
        do {
            pU = ScalarMultiply.addPoint(rU, kG);
            String PU = ScalarMultiply.setPUFromECPoint(pU);
            certificate.setPU(PU);
            certificate.setSignature(BigInteger.ZERO);

            //content of certificate to make signature
            String tbsCert = CertificateDao.makeTBSCertificate(certificate);
            String certStr ;
            Signature ecdsa;
            ecdsa = Signature.getInstance("SHA1withECDSA", "SunEC");
            ecdsa.initSign(privKeyCA);
            byte[] baContent = tbsCert.getBytes("UTF-8");
            ecdsa.update(baContent);
            byte[] baSignature = ecdsa.sign();
            certificate.setSignature(new BigInteger(1, baSignature));
            certStr = tbsCert + (new BigInteger(1, baSignature)).toString(16);
//            System.out.println("certStr = " + certStr);

            //write cert to Certificate.json file
            fullcontent = CertificateDao.makeCertificateBuilder(certificate);
            outputFile = CertificateDao.fileWithDirectoryAssurance(
                    Paths.get(Param.CERT_PATH, getCertFileName()).toString(),
                    "Certificate.json");
            CertificateDao.writeJsonToFile(fullcontent, outputFile);

            String EncodeCertU = Base64.getEncoder().encodeToString(certStr.getBytes());   //pU is encoded in CertU
            e = ScalarMultiply.hashN(EncodeCertU);
//            System.out.println("e = " + e);
        }
        while (ScalarMultiply.addPoint(ScalarMultiply.scalmult(pU, e), Param.QCA).equals(ECPoint.POINT_INFINITY));

        BigInteger r = e.multiply(k).add(Param.DCA).mod(Param.N);
        builder.add("r", r.toString(16));
        rJsonObject = builder.build();
        outputFile = CertificateDao.fileWithDirectoryAssurance(Paths.get(Param.CERT_PATH, getCertFileName()).toString(), "r.json");
        CertificateDao.writeJsonToFile(rJsonObject, outputFile);
        CertificateDao.writeRegisteredCert(certificate.getSerialNumber());
    }

    public static String getCertFileName (){
        if (certificate == null) {
            return null;
        }
        return CertificateDao.getSerialString(certificate.getSerialNumber());
    }

    public static void deleteCreatedCert (){
        System.out.print("certificate == null");
        certificate = null;
    }
}
