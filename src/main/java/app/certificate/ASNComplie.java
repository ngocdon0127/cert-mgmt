package app.certificate;

import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class ASNComplie {

    //create to be sign cert
    public static String getTBSCert(Certificate cert){
        //TBS = mestype + serial + curve + hash + issuer + validfrom + vadlidduration
        // + subjectID + usage + PU + argorithm + email
        String TBSCert = convertStringToHex("1")
                + CertificateDao.getSerialString(cert.getSerialNumber())
                + convertStringToHex("1")
                + convertStringToHex("1") + "a1a2a3a4a5a6a7a8" +
                getValidFrom(cert)
                + getValidDuration(cert) + getSubjectID(cert)
                + convertStringToHex(cert.getKeyUsage().toString())
                + cert.getPU()
                + convertStringToHex("0") +
                getEmail(cert);
        return TBSCert;
    }

    //getValidFrom block String
    private static String getValidFrom(Certificate cert){
        String validFrom = cert.getValidFrom().toString(16);
        while (validFrom.length() < 10 ) {
            validFrom = '0' + validFrom;
        }
        return validFrom;
    }

    //get ValidDuration block String
    private static String getValidDuration(Certificate cert){
        String validDuration = cert.getValidDuration().toString(16);
        while (validDuration.length() < 8 ) {
            validDuration = '0' + validDuration;
        }
        return validDuration;
    }

    //get subjectID block string
    private static String getSubjectID (Certificate cert) {
        String subject = cert.getSubject().toString(16);
        while (subject.length() < 16 ) {
            subject = '0' + subject;
        }
        return subject;
    }

    //get Email block String
    private static  String getEmail (Certificate cert) {
        int len = cert.getEmail().length();
        String lenStr = String.valueOf(len);
        while (lenStr.length() < 2) {
            lenStr = "0" + lenStr;
        }
        return lenStr + convertStringToHex(cert.getEmail());
    }

    //convert String to Hex
    public static String convertStringToHex(String str){
        char[] chars = str.toCharArray();
        StringBuffer hex = new StringBuffer();
        for(int i = 0; i < chars.length; i++){
            hex.append(Integer.toHexString((int)chars[i]));
        }

        return hex.toString();
    }

    //convert hex to string
    public static String convertHexToString(String hex){
        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }
//        System.out.println("Decimal : " + temp.toString());
        return sb.toString();
    }

    //write cert String to file text
    public static void writeCertStr (String certContent, File file) throws FileNotFoundException {
        // write the content in file
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(certContent.getBytes());
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    //read cert String from file text
    public static void readCertStr (File file) throws  FileNotFoundException, IOException {
        byte[] fileContent = Files.readAllBytes(file.toPath());
        System.out.println("byte[] = " + Hex.encodeHexString( fileContent ));
    }


}
