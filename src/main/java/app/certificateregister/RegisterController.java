package app.certificateregister;

import app.certificate.CertificateController;
import app.certificaterequest.CertificateRequest;
import app.certificaterequest.CertificateRequestController;
import app.login.LoginController;
import app.util.Path;
import app.util.ScalarMultiply;
import app.util.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static app.util.RequestUtil.*;

public class RegisterController {
    public static Route serveCertRegisterPage = (Request request, Response response) -> {
        LoginController.ensureUserIsLoggedIn(request, response);
        Map<String, Object> model = new HashMap<>();
        return ViewUtil.render(request, model, Path.Template.REGISTRATION);
    };

    public static Route handleCertRegisterPost = (Request request, Response response) -> {
        LoginController.ensureUserIsLoggedIn(request, response);
        Map<String, Object> model = new HashMap<>();

        //check request
        if (!CertificateRequestController.checkArgumentLength(request) || !CertificateRequestController.checkCharacters(request) ){
            model.put("RegisterFailed", true);
            return ViewUtil.render(request, model, Path.Template.REGISTRATION);
        }

        //check character
        if (!CertificateRequestController.checkRU(getQueryRegxru(request), getQueryRegyru(request))){
            model.put("WrongArgument", true);
            return ViewUtil.render(request, model, Path.Template.REGISTRATION);
        }
        //check RU on Elliptic Curve
        BigInteger x = new BigInteger(getQueryRegxru(request), 16);
        BigInteger y = new BigInteger(getQueryRegyru(request), 16);
        if (!ScalarMultiply.checkECCPoint(x, y)){
            model.put("wrongValue", true);
            return ViewUtil.render(request, model, Path.Template.REGISTRATION);
        }

        CertificateRequest certRequest = new CertificateRequest();
        certRequest.setCommonName(new BigInteger(getQueryRegCommonName(request),16));
        certRequest.setEmail(getQueryRegEmail(request));
        certRequest.setXrU(x);
        certRequest.setYrU(y);
        CertificateController.generate(certRequest);
        model.put("RegisterSuccessed", true);
        return ViewUtil.render(request, model, Path.Template.REGISTRATIONSUCCESS);
    };
}
