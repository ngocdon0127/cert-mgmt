package app.util;

import lombok.*;

public class Path {

    // The @Getter methods in order to access
    // the variables from Velocity Templates
    public static class Web {
        @Getter public static final String START = "/";
        @Getter public static final String INDEX = "/index/";
        @Getter public static final String LOGIN = "/login/";
        @Getter public static final String LOGOUT = "/logout/";
        @Getter public static final String REGISTRATION = "/registration/";
        @Getter public static final String DOWNLOAD = "/registration/download/";
        @Getter public static final String CR_DELETE = "/crlmanage/:serial/";
        @Getter public static final String CRL_MANAGEMENT = "/crlmanage/";
        @Getter public static final String CRL_DOWNLOAD = "/crldownload/";
        @Getter public static final String CA_PUB_DOWNLOAD = "/capubdownload/";
        @Getter public static final String CERTS_DOWNLOAD = "/certificatesdownload/";
        @Getter public static final String CERT_CLIENT_DOWNLOAD = "/certificateclientdownload/";
    }

    public static class Template {
        public static final String START = "/velocity/start/start.vm";
        public final static String INDEX = "/velocity/index/index.vm";
        public final static String LOGIN = "/velocity/login/login.vm";
        public static final String NOT_FOUND = "/velocity/notFound.vm";
        public static final String REGISTRATION = "/velocity/certificate/registration.vm";
        public static final String CRL_MANAGEMENT = "/velocity/certificaterevocationlist/crlmanagement.vm";
        public static final String REGISTRATIONSUCCESS = "/velocity/certificate/registersuccess.vm";
    }

}
