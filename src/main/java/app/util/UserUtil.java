package app.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.user.User;

import org.mindrot.jbcrypt.*;

public class UserUtil {

    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static List<User> readUsers() {
        List users = new ArrayList<User>();
        try {
            String json = FileUtil.readWholeFile(Paths.get(System.getProperty("user.dir"), "users.json").toString());
            List userInfos = gson.fromJson(json, ArrayList.class);
            // System.out.println(userInfos);
            for (int i = 0; i < userInfos.size(); i++) {
                Map m = (Map) userInfos.get(i);
                users.add(new User(m.get("userName") + "", m.get("salt") + "", m.get("hashedPassword") + "",
                        Float.valueOf(m.get("isManager") + "").intValue()));
            }
            // System.out.println(this.users);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return users;

    }

    public static void writeUsers(List<User> users) {
        List<Map> userInfos = new ArrayList<Map>();
        for(User user : users) {
            Map m = new HashMap();
            m.put("userName", user.getUsername());
            m.put("salt", user.getSalt());
            m.put("hashedPassword", user.getHashedPassword());
            m.put("isManager", user.getIsCAManager());
            userInfos.add(m);
        }
        try {
            FileUtil.writeFile(gson.toJson(userInfos), Paths.get(System.getProperty("user.dir"), "users.json").toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static User generateUser(String userName, String password, boolean isManager) {
        String salt = BCrypt.gensalt();
        // System.out.println("salt: " + salt);
        // salt = "$2a$10$e0MYzXyjpJS7Pd0RVvHwHe";
        // System.out.println("salt: " + salt);
        String hashedPassword = BCrypt.hashpw(password, salt);

        User user = new User(userName, salt, hashedPassword, isManager ? 1 : 0);
        // System.out.println(gson.toJson(user));
        return user;
    }

    public static void addUser(String userName, String password, boolean isManager) {
        List<User> users = readUsers();
        users.add(generateUser(userName, password, isManager));
        writeUsers(users);
    }

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage:");
            System.out.println("./addUser.sh userName password isManager");
            System.out.println("Example: ./addUser.sh user1 123123 1");
            return;
        }
        String userName = args[0];
        String password = args[1];
        boolean isManager = "1".equals(args[2]);
        addUser(userName, password, isManager);
        System.out.println("User " + userName + " added.");
    }
}