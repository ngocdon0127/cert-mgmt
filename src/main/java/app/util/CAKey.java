package app.util;

import java.security.*;
import java.security.interfaces.ECPublicKey;
import java.security.spec.*;

public class CAKey {

    public static ECPoint getRootPoint() throws Exception{
        KeyPairGenerator kpg;
        kpg = KeyPairGenerator.getInstance("EC", "SunEC");
        ECGenParameterSpec ecsp;
        ecsp = new ECGenParameterSpec("secp192r1");
        kpg.initialize(ecsp);
        ECParameterSpec Parameters = ((ECPublicKey) kpg.generateKeyPair().getPublic()).getParams();
        ECPoint G = Parameters.getGenerator();
        return G;
    }

    public static PublicKey getCAPublicKey() throws Exception{
        KeyPairGenerator kpg;
        kpg = KeyPairGenerator.getInstance("EC", "SunEC");
        ECGenParameterSpec ecsp;
        ecsp = new ECGenParameterSpec("secp192r1");
        kpg.initialize(ecsp);
        ECParameterSpec Parameters = ((ECPublicKey) kpg.generateKeyPair().getPublic()).getParams();
        ECPublicKeySpec caPublicKeySpec = new ECPublicKeySpec(Param.QCA, Parameters);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey caPublicKey = keyFactory.generatePublic(caPublicKeySpec);
        return caPublicKey;
    }

    public static PrivateKey getCAPrivateKey() throws Exception{
        KeyPairGenerator kpg;
        kpg = KeyPairGenerator.getInstance("EC", "SunEC");
        ECGenParameterSpec ecsp;
        ecsp = new ECGenParameterSpec("secp192r1");
        kpg.initialize(ecsp);
        ECParameterSpec Parameters = ((ECPublicKey) kpg.generateKeyPair().getPublic()).getParams();
        ECPrivateKeySpec privkeySpec = new ECPrivateKeySpec(Param.DCA, Parameters);
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PrivateKey privKeyCA = keyFactory.generatePrivate(privkeySpec);
        return privKeyCA;
    }
}
