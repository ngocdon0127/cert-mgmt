package app.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.ECPoint;

public class ScalarMultiply {
    private static final BigInteger ONE = new BigInteger("1");
    private static BigInteger TWO = new BigInteger("2");
    private static BigInteger THREE = new BigInteger("3");
    private static BigInteger FOUR = new BigInteger("4");

    public static ECPoint scalmult(ECPoint P, BigInteger kin) {

        ECPoint R = ECPoint.POINT_INFINITY, S = P;
        BigInteger k = kin.mod(Param.P);
        int length = k.bitLength();
        byte[] binarray = new byte[length];
        for (int i = 0; i <= length - 1; i++) {
            binarray[i] = k.mod(TWO).byteValue();
            k = k.divide(TWO);
        }

        for (int i = length - 1; i >= 0; i--) {
            R = doublePoint(R);
            if (binarray[i] == 1) {
                R = addPoint(R, S);
            }
        }
        return R;
    }

    public static ECPoint addPoint(ECPoint r, ECPoint s) {

        if (r.equals(s)) {
            return doublePoint(r);
        } else if (r.equals(ECPoint.POINT_INFINITY)) {
            return s;
        } else if (s.equals(ECPoint.POINT_INFINITY)) {
            return r;
        }
        BigInteger slope = (r.getAffineY().subtract(s.getAffineY())).multiply(r.getAffineX().subtract(s.getAffineX()).modInverse(Param.P)).mod(Param.P);
        BigInteger Xout = (slope.modPow(TWO, Param.P).subtract(r.getAffineX())).subtract(s.getAffineX()).mod(Param.P);
        BigInteger Yout = s.getAffineY().negate().mod(Param.P);// gia tri -xS mod p
        Yout = Yout.add(slope.multiply(s.getAffineX().subtract(Xout))).mod(Param.P);
        ECPoint out = new ECPoint(Xout, Yout);
        return out;
    }

    public static ECPoint doublePoint(ECPoint r) {
        if (r.equals(ECPoint.POINT_INFINITY)) {
            return r;
        }
        BigInteger slope = (r.getAffineX().pow(2)).multiply(new BigInteger("3"));
        slope = slope.add(Param.A);
        slope = slope.multiply((r.getAffineY().multiply(TWO)).modInverse(Param.P));
        BigInteger Xout = slope.pow(2).subtract(r.getAffineX().multiply(TWO)).mod(Param.P);
        BigInteger Yout = (r.getAffineY().negate()).add(slope.multiply(r.getAffineX().subtract(Xout))).mod(Param.P);
        ECPoint out = new ECPoint(Xout, Yout);
        return out;
    }

    //check a point is a point in EC, return true if (x,y) is a point in EC
    public static boolean checkECCPoint(BigInteger x, BigInteger y){
        BigInteger right = x.modPow(THREE, Param.P).add(x.multiply(Param.A).mod(Param.P)).add(Param.B).mod(Param.P);
        BigInteger left =  y.modPow(TWO, Param.P);
        if (left.equals(right)) {
            return true;
        }
        return false;
    }

    //getPU from String
    public static ECPoint getPUFromString(String PU) {
        String bit = PU.substring(0,1);
        int sign = Integer.parseInt(bit);
        String x = PU.substring(1);
        BigInteger X = new BigInteger(x,16);
        BigInteger right = X.modPow(THREE, Param.P).add(X.multiply(Param.A)).add(Param.B).mod(Param.P);
        BigInteger Y = right.modPow(((Param.P.add(ONE)).divide(FOUR)),Param.P);
        //check y
        if (Y.mod(TWO).intValue() != sign) {
            Y = Y.negate().mod(Param.P);
        }
        ECPoint pU = new ECPoint(X,Y);
        return pU;
    }
    
    public static String setPUFromECPoint(ECPoint pU){
        String PU;
        BigInteger sign = pU.getAffineY().mod(TWO);
        PU = sign.toString(16);
        PU += pU.getAffineX().toString(16);
       // System.out.println("PU = " + PU);
        return PU;
    }

    public static BigInteger hashN(String string) {
        try {
            // Static getInstance method is called with hashing SHA
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] messageDigest = md.digest(string.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            BigInteger h = new BigInteger(hashtext, 16);
            return h.mod(Param.N);
        } // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown"
                    + " for incorrect algorithm: " + e);
            return null;
        }
    }

    //function to compute random k < n
    public static BigInteger getRandomBigInteger() {
        SecureRandom rand = new SecureRandom();
        BigInteger upperLimit = Param.N;
        BigInteger result;
//        System.out.println("upperlimit = " + upperLimit.bitLength());
        do {
            result = new BigInteger(upperLimit.bitLength(), rand);
        } while (result.compareTo(upperLimit) >= 0);
        return result;
    }
}
