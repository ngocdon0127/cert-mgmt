package app.util;

import java.math.BigInteger;
import java.nio.file.Paths;
import java.security.spec.ECPoint;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class Param {
    // public static BigInteger DCA = new BigInteger("DCA",16);
    // public static BigInteger X_QCA = new BigInteger("X_QCA",16);
    // public static BigInteger Y_QCA = new BigInteger("Y_QCA",16);
    // public static ECPoint QCA = new ECPoint( X_QCA,Y_QCA);

    public static BigInteger DCA = null;
    public static BigInteger X_QCA = null;
    public static BigInteger Y_QCA = null;
    public static ECPoint QCA = null;
    

    public final static BigInteger P = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF", 16);
    public final static BigInteger N = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFF99DEF836146BC9B1B4D22831", 16);
    public final static BigInteger A = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFC", 16);
    public final static BigInteger B = new BigInteger("64210519E59C80E70FA7E9AB72243049FEB8DEECC146B9B1", 16);


    public final static BigInteger ISSUER = new BigInteger("a1a2a3a4a5a6a7a8",16);

    public final static String CERT_PATH = Paths.get(System.getProperty("user.dir"), "certificates").toString();
   //         System.getProperty("user.dir") + "\\certificate\\";

    //public final static String CERT_PATH = System.getProperty("user.dir") + "\\certificate\\";
    public final static String CRL_PATH = Paths.get(
            System.getProperty("user.dir"), "crl","CRL.json").toString();

    public final static String CA_PUB_PATH = Paths
            .get(System.getProperty("user.dir"), "CApub.json").toString();

    public final static String CERT_CLIENT_PATH = Paths.get(
            System.getProperty("user.dir"), "download","certificateClient.zip").toString();
    public final static String CRL_DIRECTORY = Paths.get(System.getProperty("user.dir"), "crl").toString();

    public final static String REGISTED_CERTIFICATE_DIRECTORY = Paths.get(System.getProperty("user.dir"),
            "regCert").toString();

    public final static String REGISTED_CERTIFICATE_PATH =
            Paths.get(System.getProperty("user.dir"), "regCert","RegistedCertificate.json").toString();

    public static void init() throws Exception {
        String json = FileUtil.readWholeFile(Paths.get(System.getProperty("user.dir"), "CApriv.json").toString());
        // System.out.println("jsonStr = " + json);
        Gson gson = new Gson();
        Map m = gson.fromJson(json, HashMap.class);
        // System.out.println("Map = " + gson.toJson(m));
        DCA = new BigInteger(m.get("DCA") + "",16);

        json = FileUtil.readWholeFile(Paths.get(System.getProperty("user.dir"), "CApub.json").toString());
        // System.out.println("jsonStr = " + json);
        
        m = gson.fromJson(json, HashMap.class);
        // System.out.println("Map = " + gson.toJson(m));
        X_QCA = new BigInteger(m.get("X_QCA") + "",16);
        Y_QCA = new BigInteger(m.get("Y_QCA") + "",16);
        QCA = new ECPoint( X_QCA,Y_QCA);
    }

    public static class MESType {
        public final static Integer T1 = 0;
        public final static Integer T2 = 1;
    }


    public static class CurveType {
        public static Integer SECP192K1 = 0;
        public static Integer SECP192R1 = 1;
        public static Integer SECP224K1 = 2;
        public static Integer SECP224R1 = 3;
        public static Integer SECP256K1 = 4;
        public static Integer SECP256R1 = 5;
        public static Integer SECP384R1 = 6;
        public static Integer SECP512R1 = 7;
    }

    public static class HashParam{
        public final static Integer SHA224 = 0;
        public final static Integer SHA256 = 1;
        public final static Integer SHA384 = 2;
        public final static Integer SHA512 = 3;
        public final static Integer SHA1withECDSA = 4;
    }

    public static class KeyUsage {
        public final static Integer digitalSignature = 0;
        public final static Integer nonRepudiation = 1;
        public final static Integer keyEncipherment = 2;
        public final static Integer dataEncipherment = 3;
        public final static Integer keyAgreement = 4;
        public final static Integer keyCertSign = 5;
        public final static Integer cRLSign  = 6;
    }

    public static class KeyAlgorithm{
        public final static Integer ECC_192 = 0;
    }

}


