package app.certificaterequest;

import java.math.BigInteger;

public class CertificateRequest {
    private BigInteger commonName;
    private String email = "";
    private BigInteger xrU = BigInteger.ZERO;
    private BigInteger yrU = BigInteger.ZERO;

    public BigInteger getCommonName() {
        return commonName;
    }

    public void setCommonName(BigInteger commonName) {
        this.commonName = commonName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigInteger getXrU() {
        return xrU;
    }

    public void setXrU(BigInteger xrU) {
        this.xrU = xrU;
    }

    public BigInteger getYrU() {
        return yrU;
    }

    public void setYrU(BigInteger yrU) {
        this.yrU = yrU;
    }


}
