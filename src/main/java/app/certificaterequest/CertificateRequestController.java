package app.certificaterequest;

import spark.Request;


import static app.util.RequestUtil.*;

public class CertificateRequestController {

    //function to check length of ic register argument
    public static boolean checkArgumentLength(Request request) {
        if (getQueryRegCommonName(request).length() > 0 &&
                 getQueryRegEmail(request).length() > 0 &&
                getQueryRegxru(request).length() > 0 && getQueryRegyru(request).length() > 0 &&
                getQueryRegCommonName(request).length() <= 16  &&
                 getQueryRegEmail(request).length() <= 126 &&
                getQueryRegxru(request).length() <= 48 && getQueryRegyru(request).length() <= 48)
        {
            return true;
        }
        else return false;
    }

    //function to check characters argument
    public static boolean checkCharacters(Request request) {
        String pattern = "^[0-9A-Fa-f]+$";
        if (getQueryRegCommonName(request).matches(pattern))
        {
//            System.out.println("Tham so dung");
            return true;
        }
        else return false;
    }

    //function to check certificate request argument
    public static boolean checkRU(String xrU, String yrU) {
        String pattern = "^[0-9A-Fa-f]+$";
        if (xrU.matches(pattern) && yrU.matches(pattern)) {
            return true;
        } else {
            return false;
        }
    }


}
