package app;

import app.certificateregister.RegisterController;
import app.certificaterevocationlist.CertificateRevocationController;
import app.certificaterevocationlist.CertificateRevocationDao;
import app.download.DownloadController;
import app.index.IndexController;
import app.index.StartController;
import app.login.LoginController;
import app.user.UserDao;
import app.util.Filters;
import app.util.Param;
import app.util.Path;
import app.util.ViewUtil;

import static spark.Spark.*;
//import static spark.debug.DebugScreen.*;

public class Application {

    // Declare dependencies
    public static UserDao userDao;
    public static CertificateRevocationDao certificateRevocationDao;

    public static void main(String[] args) throws Exception {
        // Init CA params
        Param.init();
        // Instantiate your dependencies
        userDao = new UserDao();
        certificateRevocationDao = new CertificateRevocationDao();

        // Configure app
        if (args.length == 0)
            port(4567);
        else
            port (Integer.parseInt(args[0]));

        staticFiles.location("/public");
        staticFiles.expireTime(600L);
//        enableDebugScreen();

        // Set up before-filters (called before each get/post)
        before("*",                  Filters.addTrailingSlashes);
        before("*",                  Filters.handleLocaleChange);

        // Set up routes
        get(Path.Web.START,            StartController.serveStartPage);
        get(Path.Web.INDEX,            IndexController.serveIndexPage);
        get(Path.Web.LOGIN,            LoginController.serveLoginPage);
        post(Path.Web.LOGIN,           LoginController.handleLoginPost);
        post(Path.Web.LOGOUT,          LoginController.handleLogoutPost);
        get(Path.Web.REGISTRATION,     RegisterController.serveCertRegisterPage);
        post(Path.Web.REGISTRATION,    RegisterController.handleCertRegisterPost);
        get(Path.Web.DOWNLOAD,         DownloadController.getFile);
        get(Path.Web.CRL_MANAGEMENT,   CertificateRevocationController.fetchAllCR);
        post(Path.Web.CRL_MANAGEMENT,  CertificateRevocationController.handleCRLPost);
        get(Path.Web.CR_DELETE,        CertificateRevocationController.handleCRDelete );
        get(Path.Web.CRL_DOWNLOAD,     DownloadController.handleCRLDownload);
        get(Path.Web.CA_PUB_DOWNLOAD, DownloadController.handleCAPubDownload);
        get(Path.Web.CERTS_DOWNLOAD,     DownloadController.handleCertificatesDownload);
        get(Path.Web.CERT_CLIENT_DOWNLOAD,     DownloadController.handleCertificateClientDownload);


        get("*",                     ViewUtil.notFound);

        //Set up after-filters (called after each get/post)
        after("*",                   Filters.addGzipHeader);

    }

}
